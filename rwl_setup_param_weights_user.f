      subroutine rwl_setup_params_weights_user(count)
      implicit none
      integer count
      integer, save :: old_pdf1,old_pdf2,old_facfact,old_renfact
      include 'pwhg_rwl.h'
      include 'pwhg_pdf.h'
      include 'pwhg_flg.h'
      include 'pwhg_st.h'
      logical rwl_keypresent
      real * 8 val
      character * 5 scheme
      integer iorder,iret
      logical, save :: ini=.true., for_reweighting
      real * 8 powheginput
      if(ini) then
         for_reweighting = powheginput("#for_reweighting") == 1
         ini = .false.
      endif
      if(count==0) then
         continue
      elseif(count == -1) then
         if(for_reweighting) then
            flg_for_reweighting = .true.
            flg_novirtual = .true.
         endif
      else
         flg_for_reweighting = .false.
         flg_novirtual = .false.         
      endif
      end
