      subroutine setborn(p,bflav,born,bornjk,bmunu)
c Wrapper subroutine to call OL Born
      use openloops_powheg, only: ol_born => openloops_born
      implicit none
      include 'nlegborn.h'
      integer, parameter :: nlegs=nlegborn
      real * 8, intent(in)  :: p(0:3,nlegs)
      integer,  intent(in)  :: bflav(nlegs)
      real * 8, intent(out) :: born
      real * 8, intent(out) :: bornjk(nlegs,nlegs)
      real * 8, intent(out) :: bmunu(0:3,0:3,nlegs)

      integer i
      real * 8 dotp
      external dotp
      logical :: debug = .false.

      if(debug) then
      print*, "==="
      do i=1,nlegs
        print*, p(:,i), sqrt(dotp(p(:,i),p(:,i)))
      end do
      end if
      call ol_born(p,bflav,born,bornjk,bmunu)
      if(debug) then
        print*, born
      end if
      end subroutine setborn


      subroutine borncolour_lh
c Wrapper subroutine to call the OL code to associate
c a (leading) color structure to an event.
      use openloops_powheg
      implicit none
      include 'nlegborn.h'
      include 'LesHouches.h'
      include 'pwhg_rad.h'
      include 'pwhg_flst.h'
      include 'pwhg_kn.h'
      real * 8 p(0:3,nlegborn)
      integer bflav(nlegborn),is_fs_flav(nlegborn),
     1     is_fs(nlegborn),isfslength,color(2,nlegborn)
      integer j

      do j=1,nlegborn
        bflav(j)=idup(j)
        if (bflav(j).eq.21) bflav(j)=0
        p(:,j) = kn_cmpborn(:,j)
      enddo

      ! call OL colorflow
      call openloops_borncolour(p,bflav,color)


      do j=1,nlegborn
        icolup(:,j)=color(:,j)
      enddo
      
      end

      subroutine finalize_lh
      implicit none
c adapted from POWHEG-BOX/ttJ:
      include 'nlegborn.h'
      include 'PhysPars.h'
      include 'LesHouches.h'
      include 'pwhg_flg.h'
      include 'pwhg_kn.h'
      real * 8 powheginput
      external powheginput
      real * 8 ptop(0:3),ptbar(0:3),pb(0:3),pbbar(0:3)
      real * 8 mt_top,mt_tbar,mt_b,mt_bbar,mt_g
      real * 8 LOscalupHTPF

c test only:
c      return

c     consistency check
      if ((nup.ne.6).and.(nup.ne.7)) then
         write(*,*) "Error in finalize_lh, nup=",nup
         write(*,*) "Only nup=6 or 7 allowed at this stage"
         write(*,*) "Halting execution"
         call exit(-1)
      endif
c     Performs the decay of top and anti-top quarks
      call randomsave
      call ttbb_decay
      call randomrestore
c
c     give masses to final-state products
c     TODO, this should probably be disabled -- I check it makes difference on the ~6th digit (in one event)
c      call lhefinitemasses

c     overwrite the default scalup for LOevents
      if(powheginput("#LOscalupHT").eq.1.and.flg_LOevents) then
        LOscalupHTPF = powheginput("#LOscalupHTPF")
        if (LOscalupHTPF < 0) LOscalupHTPF = 1
        ptop(:)   = kn_pborn(:,3) !t
        ptbar(:)  = kn_pborn(:,4) !t~
        pb(:)     = kn_pborn(:,5) !b
        pbbar(:)  = kn_pborn(:,6) !b~
        call gettransmass(ph_tmass,ptop,mt_top)
        call gettransmass(ph_tmass,ptbar,mt_tbar)
        call gettransmass(ph_bmass,pb,mt_b)
        call gettransmass(ph_bmass,pbbar,mt_bbar)
        scalup = 0.5*(mt_top+mt_tbar+mt_b+mt_bbar)*LOscalupHTPF
      endif

      end
