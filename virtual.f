      subroutine setvirtual(p,vflav,virtual)
c Wrapper subroutine to call OL Virtual
      use openloops_powheg, only: ol_born => openloops_born, ol_virtual => openloops_virtual
      implicit none
      include 'nlegborn.h'
      include 'pwhg_st.h'
      include 'pwhg_math.h'
      include 'PhysPars.h'
      integer, parameter :: nlegs=nlegborn
      real * 8, intent(in)  :: p(0:3,nlegs)
      integer,  intent(in)  :: vflav(nlegs)
      real * 8, intent(out) :: virtual
      real * 8 ::  born

      logical, save :: ini = .true., MSbarscheme = .false. !! TODO: decide to which value set by default
      real *8 :: powheginput
      external powheginput

      call ol_virtual(p,vflav,virtual,born)


      if (ini) then
        if (powheginput("#MSbarscheme").eq.0) MSbarscheme=.false.
        if (powheginput("#MSbarscheme").eq.1) MSbarscheme=.true.
        ini=.false.
      endif

      if (MSbarscheme) then
c     see  Cacciari, Greco, Nason  hep-ph/9803400
         if (vflav(1).ne.0.and.vflav(2).ne.0) then
c     a factor of as/(2 pi) will be attached by the BOX
            virtual=virtual - 4d0/3*TF*log(st_muren2/ph_bmass**2)*born
         elseif  (vflav(1).eq.0.and.vflav(2).eq.0) then
c     a factor of as/(2 pi) will be attached by the BOX
            virtual = virtual + 4d0/3*TF*log(st_mufact2/st_muren2)*born
         else
            write(*,*) "We should not be here!!"
            call pwhg_exit(-1)
         endif
      endif


      end
